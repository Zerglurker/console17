#pragma once
#include<string>

class Animal
{
protected:
	virtual std::string GetProperty() { return ""; }
public:
	virtual void Voice() {}
	void PrintDescription();
};

class Dog :	public Animal
{
	std::string Owner;
protected:
	std::string GetProperty();
public:
	Dog();
	Dog(std::string);

	void Voice() override;
};

class Cat : public Animal {
	double speed=0;
protected:
	std::string GetProperty();
public:
	
	void Voice() override;
};

class Mouse : public Animal {
	int size=10;
protected:
	std::string GetProperty();
public:
	void Voice() override;
};