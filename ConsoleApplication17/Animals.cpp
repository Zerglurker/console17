#include "Animals.h"
#include<iostream>
#include<string>


using namespace std;

void Animal::PrintDescription() {
	cout << GetProperty() << '\n';
}


Dog::Dog() : Owner("") {}

Dog::Dog(string owner_) : Owner(owner_) {}

void Dog::Voice()
{
	cout << "Woof!\n";
}

string Dog::GetProperty() { 
	if (Owner.empty()) {
		return "This dog is stray";
	}
	else {
		return "This dog has owned by " + Owner; //owner of this dog 
	}
}

void Cat::Voice()
{
	cout << "May\n";
}

string Cat::GetProperty() {
	if (speed==0) {
		return "This Cat is sleep";
	}
	else {
		return "This Cat is runned at " + to_string(speed); 
	}
}

string Mouse::GetProperty() {
	return "size="+to_string(size)+"mm";
}

void Mouse::Voice() {
	cout << "Pc-c-c-c\n";
}
