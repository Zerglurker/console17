#pragma once
#include<stdexcept>

template <class T>
class Stack{
private:
	T* a;
	long size;
	long pos;

	void resize(long size_)
	{
		//���� ������ ���� ������ �� ��������
		T* new_a = new T[size_];

		//for (auto& item : a) 
		for (long i = 0; i < pos; i++) {
			*(new_a + i) = *(a + i);
		};

		delete a;

		a = new_a;
		size = size_;

	}

public:
	Stack() : size(10), pos(-1) { a = new T[size]; };
	T pop() {
		if (pos >= 0) {
			return *(a + pos--);
		}
		else {
			throw std::out_of_range("empty stack!");
		}
	}

	void push(T item) {
		if (++pos >= size) {
			resize(size + 10);
		};

		*(a + pos) = item;

	}

	long length() {
		return pos+1;
	};
};