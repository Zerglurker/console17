#pragma once

#include <iostream>
#include <cmath>
#include "Vector.h"

Vector::Vector() : x(0), y(0), z(0) {}
Vector::Vector(double x_, double y_, double z_) : x(x_), y(y_), z(z_) {}

void Vector::Show()
{
	std::cout << '(' << x << ';' << y << ';' << z << ")\n";
}

double Vector::lenght() {
	return sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
}